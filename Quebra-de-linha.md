# Parágrafo

Quebra de linha é quando vc quer que a próxima linha fiquem embaixo.

Existem várias maneiras.

- Se vc der dois espaços ele passa para a próxima linha
- Se vc utilizar uma \ também é possível, mas nem todos os aplicativos que suportam markdown aceitam isso. (evite).
- tag `<br>` tb passa par a próxima linha.

Selecione o texto abaixo para ver os espaços em branco

```markdown
Iniciando um texto  
continuando  
e vai continuando ainda mais.
```

```html
Iniciando um texto <br>
continuando <br>
e vai continuando ainda mais.
```

Isso também seria possível apesar de não recomendado.

```markdown
Iniciando um texto \
continuando \
e vai continuando ainda mais.
```
