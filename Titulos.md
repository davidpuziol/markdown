# Titulos

No Mardown **.md** a primeira coisa que vc precisa saber é a usar o `#`.

Com ele vc gera uma árvore de títulos. Pensem e pastas no seu explorador de arquivos, é a mesma coisa, porém só existe uma pasta raiz para o restante do projeto. Logo vc só pode ter um título principal.

>Você pode ter mais de um título principal? Sim. É boa prática? Não.

Vejo uma certa confusão de pessoas achando que o `#` ou `##` ou `<h3>` é simplesmente para mudar o tamanho da fonte do título. Na verdade isso define níveis de título e por geralmente um subtítulo tem fonte menor do que o seu título de herança. Na verdade o tamanho da fonte é mais para vc se encontrar no texto e saber se esta especializando no assunto ou passando para um próximo.

A marcação de um título precisa ter `um espaço entre # e a primeira palavra`.

É boa prática `pular uma linha depois dos títulos`.

```md
# Titulo

## Subtítulo1

<h3> Subsubtítulo1 1

<h3> Subsubtítulo1 2

## Subtítulo2

<h3> Subsubtítulo2 1

<h3># Subsubtítulo2 1 1

<h3> Subsubtítulo2 2
```

Porém também é possível usar marcações html para títulos.

```md
<h1> Titulo </h1>

<h2> Subtítulo1 </h2>

<h3> Subsubtítulo1 1 </h3>

<h3> Subsubtítulo1 2 </h3>

<h2> Subtítulo2 </h2>

<h3> Subsubtítulo2 1 </h3>

<h4> Subsubtítulo2 1 1 </h4>

<h3> Subsubtítulo2 2 </h3>
```
