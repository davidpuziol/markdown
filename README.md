# Markdown

A idéia desse repositório é ajudar que todos façam suas documentações da melhor forma possível.

Ao longo do caminho fui percebendo o valor de documentar tudo que fazemos. A documentação parece uma perda de tempo no início, mas depois percebemos o quanto ganhamos tempo. Na minha profissão de devops muitas coisas precisam ser documentadas, pois nem sempre lembramos tudo que fazemos. São tantas ferramentas, tantos cenários, tantas coisas que é bom anotar tudo.

Documentar tem suas vantagens:

- Fixar o conteúdo do que vc fez ou estudou.
- Passar o conhecimento adiante.
- Organizar suas idéias.
- Lembrar (Isso é muito importante, principalmente com a idade chegando).
- Portifólio
- Evitar ser requisitado por falta de entendimento do que vc fez.

Sabe quando vc utilizar uma ferramenta e a documentação esta impecável e bem escrita? Não dá um prazer de trabalhar com aquilo?

Vou tentar documentar aqui tudo o que eu encontrar ao longo do caminho para trabalhar com markdown e a minha e a sua vida.

Recomendo o uso da extensão para vscode *markdown lint* (DavidAnson.vscode-markdownlint), ajuda bastante.

[Documentação oficial](https://www.markdownguide.org/basic-syntax/)

## Recomendações

1. Consulte a documentação oficial
2. Aprenda de verdade a marcação ao invés de usar editores prontos.
3. Visual studio code dá para ver em tempo real o que vc está digitando, logo não precisa de outros editores.
