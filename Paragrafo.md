# Parágrafo

Se vc simplesmente pular de linha é gera um novo parágrafo. Tabulações de parágrafo não são uma prática com textos de markdown, mas é possível.

```md
Um exemplo de texto

Outro exemplo de texto em outro parágrafo
```

Também em html

```html
<p>Um exemplo de texto.</p>

<p>Outro exemplo de texto em outro parágrafo.</p>
```

https://www.markdownguide.org/hacks/#indent-tab
